#[macro_use] extern crate serde;

use ntex::web::{self, middleware, App};
use ntex::util::HashMap;
use ntex::http::StatusCode;

#[derive(Deserialize, Serialize)]
pub struct HelloStruct {
    name: String,
}

fn build_response(code: StatusCode, body: String) -> web::HttpResponse {
    web::HttpResponse::build(code).body(&body)
}

async fn hello_query(query: web::types::Query<HashMap<String, String>>) -> web::HttpResponse {
    match query.contains_key("q") {
        true => build_response(StatusCode::OK, format!("Hello, {}", query["q"])),
        false => build_response(StatusCode::BAD_REQUEST, String::from("Missing key: q"))
    }
}

async fn hello_path(path: web::types::Path<(String,)>) -> web::HttpResponse {
    build_response(StatusCode::OK, format!("Hello, {}", path.into_inner().0))
}

async fn hello_post(json: web::types::Json<HelloStruct>) -> web::HttpResponse {

    build_response(StatusCode::ACCEPTED, format!("Hello, {}", json.name))
}

struct AppState {
    entry: String,
}

fn app_config(config: &mut web::ServiceConfig) {
    config.service(
        web::scope("")
            .data(AppState { entry: "bar".to_string(), })
            .service((web::resource("/").route(web::get().to(|| async { "Configured." })), )),
    );
}

#[ntex::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    web::server(|| {
        App::new()
            .wrap(middleware::Logger::default())
            // .data(pool.clone())
            .app_data(web::types::JsonConfig::default().limit(4096))
            .service((
                web::resource("/").to(|| async { "Hello World!" }),
                web::resource("/static").to(|| async { web::HttpResponse::Ok()
                    .content_type("text/html; charset=utf-8")
                    .body(include_str!("../static/hello.html"))
                }),
                web::resource("/hello").to(|| async { web::HttpResponse::Ok()
                    .json( &HelloStruct { name: String::from("Hello") })
                }),
                web::resource("/req").to(|req: web::HttpRequest| async move { format!("{:?}", req) }),
                web::resource("/hello/query").route(web::get().to(hello_query)),
                web::resource("/hello/path/{p}").route(web::get().to(hello_path)),
                web::resource("/hello/post").route(web::post().to(hello_post)),
            ))
            // .configure(app_config)
    })
        .bind("127.0.0.1:3000")?
        .run()
        .await
}